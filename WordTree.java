import java.util.Scanner;

public class WordTree {
    public static void main(String[] args) {
        
        String word = evenWord();
        printTreeWord(word);
    }

    private static void printTreeWord(String word) {
        int height = word.length() / 2 + 1;
        int a = word.length() / 2;
        int z = word.length() / 2;
        for (int i = 1; i <= height; i++) {
            for (int j = height; j > i; j--) {
                System.out.print(" ");
            }
            System.out.print(word.substring(a, z + 1));
            a--;
            z++;
            System.out.println();
        }
    }

    private static String evenWord() {
        Scanner scanner = new Scanner((System.in));
        String word = "";
        boolean length = true;
        do {
            length = true;
            System.out.println("Put a word");
            word = scanner.nextLine();
            if (word.length() % 2 == 0){
                length = false;
            }
        }
        while (length == false);
        return word;
    }
}
